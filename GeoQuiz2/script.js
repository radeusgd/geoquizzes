// Disable function
jQuery.fn.extend({
    disable: function(state) {
        return this.each(function() {
            this.disabled = state;
        });
    }
});

function shuffle(array) {
    var tmp, current, top = array.length;
    if(top) while(--top) {
    	current = Math.floor(Math.random() * (top + 1));
    	tmp = array[current];
    	array[current] = array[top];
    	array[top] = tmp;
    }

    return array;
}

function preloadimages(arr, callback){
    var newimages=[], loadedimages=0;
    var arr=(typeof arr!="object")? [arr] : arr;
    function imageloadpost(){
        loadedimages++;
        if (loadedimages==arr.length){
            callback(newimages);
        }
    }
    for (var i=0; i<arr.length; i++){
        newimages[i]=new Image();
        newimages[i].src=arr[i];
        newimages[i].onload=function(){
            imageloadpost();
        };
        newimages[i].onerror=function(){
        	imageloadpost();
        };
    }
}

images = [];

quizIds = [];

currentQuizId = 0;
scoreError = 0;

function updateScores(){
	$("#maps").text(currentQuizId+1);
	$("#errs").text(scoreError);
}

function resetButtons(enabled){
	for(var i=0;i<numberOfEntries;i++){
		$("#b"+i).disable(!enabled);
	}
}

function goToQuestion(id){
	$("#next").disable(true);
	if(id>=quizIds.length){
		$("#obrazek").text("Zrestartuj quiz");
		return;
	}
	resetButtons(true);
	currentQuizId = id;
	updateScores();
	$("#obrazek").html("<img src=\""+images[quizIds[id]].src+"\" style=\"zoom: 2.0;-moz-transform: scale(0.5);\" class=\""+($("#kolor").is(":checked") ? "none" : "biw")+"\"/>");
}

function startQuiz(){
	currentQuizId = 0;
	scoreError = 0;
	quizIds = shuffle(quizIds);
	goToQuestion(0);
}

function answer(id){
	var goodAnswer = quizIds[currentQuizId];
	if(id==goodAnswer){
		resetButtons(false);
		if($("#automat").is(":checked")){
			setTimeout(function(){goToQuestion(currentQuizId+1);},350);
		}else{
			$("#next").disable(false);	
		}
		return;
	}else{
		$("#b"+id).disable(true);
		scoreError++;
		updateScores();
	}
}

var numberOfEntries = 33;

$( document ).ready(function() {
	$("#restarter").disable(true);
	for(var i=0;i<numberOfEntries;i++){
		quizIds[i]=i;
	}
	imagesToLoad = [
		"images/bambus.jpg",
		"images/bataty.jpg",
		"images/buraki.jpg",
		"images/herbata.jpg",
		"images/kauczuk.jpg",
		"images/kawa.jpg",
		"images/konopie.jpg",
		"images/kukurydza.jpg",
		"images/maniok.jpg",
		"images/oliwki.jpg",
		"images/orzeszki.jpg",
		"images/owies.jpg",
		"images/palmaoleista.jpg",
		"images/pszenica.jpg",
		"images/ryz.jpg",
		"images/sizal.jpg",
		"images/slonecznik.jpg",
		"images/sorgo.jpg",
		"images/trzcina.jpg",
		"images/ziemniaki.jpg",
		"images/zyto.jpg",
		"images/bawelna.jpg",
		"images/chmiel.jpg",
		"images/jeczmien.jpg",
		"images/juta.jpg",
		"images/kakaowiec.jpg",
		"images/kloncukrowy.jpg",
		"images/len.jpg",
		"images/palmakokosowa.jpg",
		"images/proso.jpg",
		"images/rzepak.jpg",
		"images/soja.jpg",
		"images/tyton.jpg",
			
	];
	preloadimages(imagesToLoad,function(arr){
		images = arr;
		startQuiz();
		$("#restarter").disable(false);
	});
	for(var i=0;i<numberOfEntries;i++){
		$("#b"+i).click(
		function(i){
			return function(){answer(i);};
		}(i));
	}
	$("#restarter").click(startQuiz);
	$("#next").click(function(){
		goToQuestion(currentQuizId+1);
	});
	$("#next").disable(true);
});