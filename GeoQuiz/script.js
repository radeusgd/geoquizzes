// Disable function
jQuery.fn.extend({
    disable: function(state) {
        return this.each(function() {
            this.disabled = state;
        });
    }
});

function shuffle(array) {
    var tmp, current, top = array.length;
    if(top) while(--top) {
    	current = Math.floor(Math.random() * (top + 1));
    	tmp = array[current];
    	array[current] = array[top];
    	array[top] = tmp;
    }

    return array;
}

function preloadimages(arr, callback){
    var newimages=[], loadedimages=0;
    var arr=(typeof arr!="object")? [arr] : arr;
    function imageloadpost(){
        loadedimages++;
        if (loadedimages==arr.length){
            callback(newimages);
        }
    }
    for (var i=0; i<arr.length; i++){
        newimages[i]=new Image();
        newimages[i].src=arr[i];
        newimages[i].onload=function(){
            imageloadpost();
        };
        newimages[i].onerror=function(){
        	imageloadpost();
        };
    }
}

images = [];

quizIds = [];

currentQuizId = 0;
scoreError = 0;

function updateScores(){
	$("#maps").text(currentQuizId+1);
	$("#errs").text(scoreError);
}

function resetButtons(enabled){
	for(var i=0;i<27;i++){
		$("#b"+i).disable(!enabled);
	}
}

function goToQuestion(id){
	$("#next").disable(true);
	if(id>=quizIds.length){
		$("#obrazek").text("Zrestartuj quiz");
		return;
	}
	resetButtons(true);
	currentQuizId = id;
	updateScores();
	$("#obrazek").html("<img src=\""+images[quizIds[id]].src+"\"/>");
}

function startQuiz(){
	currentQuizId = 0;
	scoreError = 0;
	quizIds = shuffle(quizIds);
	goToQuestion(0);
}

function answer(id){
	var goodAnswer = quizIds[currentQuizId];
	if(id==goodAnswer){
		resetButtons(false);
		if($("#automat").is(":checked")){
			setTimeout(function(){goToQuestion(currentQuizId+1);},350);
		}else{
			$("#next").disable(false);	
		}
		return;
	}else{
		$("#b"+id).disable(true);
		scoreError++;
		updateScores();
	}
}

$( document ).ready(function() {
	$("#restarter").disable(true);
	for(var i=0;i<27;i++){
		quizIds[i]=i;
	}
	imagesToLoad = [
		"images/pszenica.png",
		"images/ryz.png",
		"images/kukurydza.png",
		"images/jeczmien.png",
		"images/zyto.png",
		"images/owies.png",
		"images/proso.png",
		"images/sorgo.png",
		"images/ziemniaki.png",
		"images/bataty.png",
		"images/maniok.png",
		"images/trzcina.png",
		"images/buraki.png",
		"images/bawelna.png",
		"images/juta.png",
		"images/lenikonopie.png",
		"images/soja.png",
		"images/oliwki.png",
		"images/rzepak.png",
		"images/orzeszki.png",
		"images/slonecznik.png",
		"images/palmaoleista.png",
		"images/palmakokosowa.png",
		"images/tyton.png",
		"images/herbata.png",
		"images/kawa.png",
		"images/kakao.png",	
	];
	preloadimages(imagesToLoad,function(arr){
		images = arr;
		startQuiz();
		$("#restarter").disable(false);
	});
	for(var i=0;i<27;i++){
		$("#b"+i).click(
		function(i){
			return function(){answer(i);};
		}(i));
	}
	$("#restarter").click(startQuiz);
	$("#next").click(function(){
		goToQuestion(currentQuizId+1);
	});
	$("#next").disable(true);
});